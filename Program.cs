﻿using System;

namespace bedmas_example
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Question A | = (6 + 7) * (3 - 2) = {(6 + 7) * (3 - 2)}");
            Console.WriteLine($"Question B | (6 * 7) + (3 * 2) = {(6 * 7) + (3 * 2)}");
            Console.WriteLine($"Question C | (6 * 7) + 3 * 2 = {(6 * 7) + 3 * 2}");
            Console.WriteLine($"Question D | (3 * 2) + 6 * 7 = {(3 * 2) + 6 * 7}");
            Console.WriteLine($"Question E | (3 * 2) + 7 * 6 / 2 = {(3 * 2) + 7 * 6 / 2}");
            Console.WriteLine($"Question F | 6 + 7 * 3 - 2 = {6 + 7 * 3 - 2}");
            Console.WriteLine($"Question G | 3 * 2 + (3 * 2) = {3 * 2 + (3 * 2)}");
            Console.WriteLine($"Question H | (6 * 7) * 7 + 6 = {(6 * 7) * 7 + 6}");
            Console.WriteLine($"Question I | (2 * 2) + 2 * 2 = {(2 * 2) + 2 * 2}");
            Console.WriteLine($"Question J | 3 * 3 + (3 * 3) = {3 * 3 + (3 * 3)}");
            Console.WriteLine($"Question K | (6^2 + 7) * 3 + 2 = {(Math.Pow(6,2) + 7) * 3 + 2}"); // FIX SUPERSCRIPT, SAME GOES FOR ALL OTHER COMMENTS, DOUBLE MEANS FIX TWO
            Console.WriteLine($"Question L | (3 * 2) + 3^2 * 2 = {(3 * 2) + Math.Pow(3,2) * 2}"); // Math.Pow is case sensitive
            Console.WriteLine($"Question M | (6 * (7 + 7)) / 6 = {(6 * (7 + 7)) / 6}");
            Console.WriteLine($"Question N | ((2 + 2) + (2 * 2)) = {((2 + 2) + (2 * 2))}");
            Console.WriteLine($"Question O | 4 * 4 + (3^2 * 3^2) = {4 * 4 + (Math.Pow(3,2) * Math.Pow(3,2))}"); // //
        }
    }
}
